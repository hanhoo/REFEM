def conv_int(x):
    _x = x.rstrip()
    _x = _x.replace('"', '')
    return int(_x)
    
def conv_flt(x):
    _x = x.rstrip()
    _x = _x.replace('"', '')
    return float(_x)
    
def conv_str(x):
    _x = x.rstrip()
    return str(x)

def isfloat(x):
    x = x.rstrip()
    x = x.replace('"', '')
    if x.count(".") == 1:
        x = x.replace('.', '')
        if x.isalnum():
            return True
        else:
            x = x.replace('"', '')
            if x.isalnum():
                return True
    else:
        return False
    
# From the second csv field onward we have geometry attributes.
# return a list of conversion functions.
def convert_string(csv_str):
    conversion = []
    for attr in csv_str:
        # strip special characters etc.
        attr = attr.rstrip()
        attr = attr.replace('"', '')
        if len(attr) > 0:
            if attr.isdigit():
                conversion.append(conv_int)
            elif isfloat(attr):
                conversion.append(conv_flt)
            else:
                conversion.append(conv_str)
        else:
            conversion.append(conv_str)     
    return conversion

# Hardcoded! KWT is stored in the first field of the csv
# and has some default text that can be stripped off.
# Returns a list of points, seperated by a comma, consisting
# of three float coordinates. It is presumed that there are
# only two levels of nested tuples/polygons.
def convert_multipolygon(multipolygon):
    start_pos = len("MULTIPOLYGON Z ((") + 1
    end_pos = -1 * len("))") - 1
    # strip text plus first parenthesis
    stripped = multipolygon[start_pos:end_pos]
    # find different polygons in multipolygon
    stripped_length = len(stripped)
    number_polys = stripped.count("(")
    polys = []
    if number_polys == 1:
        polys.append(stripped[1:-1])
    # In case there are polygons nested in polygons,
    # adjust these lines of code
    else:
        start_ind = 0
        for i in range(number_polys):
            start_ind = stripped.find("(", start_ind)
            end_ind = stripped.find(")", start_ind)
            cur_poly = stripped[start_ind+1:end_ind]
            polys.append(cur_poly)
            stripped = stripped[:start_ind] + stripped[end_ind:]
            start_ind = 0
    return polys

# Convert the string with coordinates to a tuple of
# coordinates (3 floats).
def create_points(coordinates, line_num):
    point_accum = []
    for elem in coordinates:
        line_string = []
        point_list = elem.split(',')
        for cur_elem in point_list:
            try:
                point_pos = [float(i) for i in cur_elem.split()]
            except:
                print "Error on line %d with element %s" % (line_num + 1, cur_elem)
            point_pos = tuple(point_pos)
            line_string.append(point_pos)
        point_accum.append(line_string)
    return point_accum
    
# Create a polygon from the points    
def create_polygon(line_points, line_num):
    polygons = []
    for line_string in line_points:
        points = geo.createPoints(line_string)
        polygon = geo.createPolygon()
        for point in points:
            polygon.addVertex(point)
        polygons.append(polygon)
    return polygons
    
def make_attr_names(attr_names):
    new_attr = []
    for name in attr_names:
        name = name.rstrip()
        name = name.replace('-', '_')
        name = name.replace('.', '_')
        new_attr.append(name)
    return new_attr  
    
def add_attributes(geometry, attr_names, conversion, csv_data):
    for name, conv, data in zip(attr_names, conversion, csv_data):
        if len(data) > 0:
            # clean attribute names
            data = data.rstrip()
            def_value = conv(data)
            #print name, data, def_value, type(def_value)
            hou.pwd().geometry().addAttrib(hou.attribType.Prim, name, def_value)
        
def set_attributes(geometry, attr_names, conversion, csv_data):
    for elem in geometry:
        for name, conv, data in zip(attr_names, conversion, csv_data):
            #print name, conv, data
            try:
                if len(data) > 0:
                    # clean attribute names
                    data = data.rstrip()
                    attr_value = conv(data)
                    elem.setAttribValue(name, attr_value)
            except:
                print "Error with element %s" % csv_data[0]
                print "Could not add attribute %s" % name
                print "with value: %s" % data
    
# MAIN
geo = hou.pwd().geometry()
node = hou.pwd()

fhandle_csv = node.parm("qgis_csv").eval()
line_number = 1
attr_names = []

with open(fhandle_csv, "r") as gisData:
    for cur_line in gisData:
        gis_row = cur_line.split(';')
        # parse first line for attribute names. The first attribute should be WKT
        # aka the points of the geometry. If it is not present print a warning.
        if line_number == 1:
            if gis_row[0] != "WKT":
                raise hou.NodeError("WKT data field is missing, no geometry in csv.")
            attr_names = make_attr_names(gis_row[1:])
            line_number += 1
        # parse second line for attribute types
        elif line_number == 2:
            # create geometry
            coordinates = convert_multipolygon(gis_row[0])
            line_points = create_points(coordinates, line_number)
            plot = create_polygon(line_points, line_number)
            # create attribute conversion
            conv = convert_string(gis_row[1:])
            # create attributes, and then set attributes
            add_attributes(plot, attr_names, conv, gis_row[1:])
            set_attributes(plot, attr_names, conv, gis_row[1:])
            line_number += 1
        # parse the rest of the csv
        elif line_number > 2:
            coordinates = convert_multipolygon(gis_row[0])
            line_points = create_points(coordinates, line_number)
            plot = create_polygon(line_points, line_number)
            set_attributes(plot, attr_names, conv, gis_row[1:])
            line_number += 1